import React, { Component } from "react";

class Entrepreneur extends Component {
  state = {
    startups: [
      {
        date: "2013 - 2016",
        title: "Co-Founder",
        company: "AudiOnIt, Inc",
        descrip: "Startup technology company AudiOnIt developed a process to take voice and video messages recorded via phone or web and transferring them on to elegantly designed and manufactured audio/video cards."
      },
      {
        date: "2011 - 2012",
        title: "Co-Founder",
        company: "CountM, Inc",
        descrip: "CountM developed an egress and ingress tracking platform for various industries."
      }
      /*{
        date: "2000 - 2005",
        title: "Founder & President",
        company: "WorldNightClubs / NightLifeConnect, LLC.",
        descrip: "A social network and online marketing company for the nightclub and entertainment industries covering all of Southern California. Unique in the industry and a pioneer of its time."
      }*/
    ]
  };
  render() {
    return (
      <React.Fragment>
        <a href="#collapseEntrepreneurship" data-toggle="collapse" aria-controls="collapseEntrepreneurship" className="cv-header">
          <h2 className="title">
            <span>Entrepreneurship</span>
          </h2>
          <span className="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>
          <span className="glyphicon glyphicon-triangle-bottom" style={{ display: "none" }} aria-hidden="true"></span>
        </a>
        <div className="white-space-10"></div>

        <ul id="collapseEntrepreneurship" className="resume-list collapse">
          {this.renderStartups()}
        </ul>
      </React.Fragment>
    );
  }

  renderStartups() {
    return this.state.startups.map((startup, i) => (
      <li key={i}>
        <h4>
          <i className="fa fa-calendar ic-re"></i> {startup.date}
        </h4>
        <i>{startup.title}</i>
        <h3>
          <i className="fa fa-building-o ic-re"></i> {startup.company}
        </h3>
        <p>{startup.descrip}</p>
      </li>
    ));
  }
}

export default Entrepreneur;
