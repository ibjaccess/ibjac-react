import React, { Component } from "react";

class Skills extends Component {
  state = {
    skills: ["HTML5", "CSS3", "jQuery", "JavaScript", "Bootstrap", "SASS", "RWD", "XML", "XSL", "PhoneGap", "PHP", "MySQL", "Laravel", "SEO", "ATG", "Endeca", "Magento", "WordPress", "Drupal", "Shopify", "Shell", "AJAX", "API", "EDM", "ESP", "Apache", "SAP", "JIRA", "Angular", "Optimization", "LINUX", "SQL", "Facebook", "Android"]
  };

  render() {
    return (
      <React.Fragment>
        <h3 className="title-2">
          <span>Varying knowledge, skills, or expertise in...</span>
        </h3>
        <div id="skills" className="clearfix br-t br-r ">
          {this.dispSkills()}
        </div>
      </React.Fragment>
    );
  }

  dispSkills() {
    let allSkills = this.state.skills.map(skill => (
      <div key={skill} className="col-sm-2 col-xs-6 no-padding">
        <div className="desc-mini">
          <div className="mid-desc-mini">
            <p>{skill}</p>
          </div>
        </div>
      </div>
    ));

    return allSkills;
  }
}

export default Skills;
