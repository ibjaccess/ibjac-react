import React, { Component } from "react";

class IBeMe extends Component {
  render() {
    return (
      <React.Fragment>
        <a href="#collapseIBMe" data-toggle="collapse" aria-controls="collapseIBMe" className="cv-header">
          <h2 className="title">
            <span>I Be Me</span>
          </h2>
          <span className="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>
          <span className="glyphicon glyphicon-triangle-bottom" style={{ display: "none" }} aria-hidden="true"></span>
        </a>

        <div className="white-space-10"></div>

        <ul id="collapseIBMe" className="resume-list collapse">
          <li>
            <div className="img-pr">
              <img src="/assets/theme/img/profile.jpg" alt="J.A.C." />
            </div>
            <h3>
              <i className="fa fa-building-o ic-re"></i> In The Beginning
            </h3>
            <p>My first web development job was doing HTML coding for an interactive agency in Torrance, CA. The original plan back then was to work in a creative field as a designer, but as plans often do, they didn't go as expected. Through no conscious desire of my own, not even as a "what if," the design trajectory I had laid out for myself propelled me into the world of web development where I quickly became consumed by it...and that was a good thing.</p>

            <p>
              <a href="https://en.wikipedia.org/wiki/Arthur_Ashe" title="WikiPedia: Arthur Ashe">
                Arthur Ashe's
              </a>
              quote, "<em>Start where you are. Use what you have. Do what you can.</em>" is how I began. In the late 90's there were no web development degrees, no developer boot camps, and no online training courses. Internet access was not easy to come by but every chance there was I spent web surfing, researching, self-training and coding.
            </p>

            <p>
              A quote by
              <a href="https://en.wikipedia.org/wiki/Jason_Calacanis" title="WikiPedia: Jason Calacanis">
                Jason Calacanis
              </a>
              I feel also fits my experience quite nicely.
            </p>

            <blockquote>
              <em>You have to have a big vision and take very small steps to get there. You have to be humble as you execute but visionary and gigantic in terms of your aspiration. In the Internet industry, it's not about grand innovation, it's about a lot of little innovations: every day, every week, every month, making something a little bit better.</em>
            </blockquote>

            <p>
              This has been my journey and what still excites me about what I do is that as I try to work on small innovations every day to make what I'm working on just a little better, that same process is making <i>me</i> a little better as well.
            </p>
          </li>
        </ul>
      </React.Fragment>
    );
  }
}

export default IBeMe;
