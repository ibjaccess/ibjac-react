import React, { Component } from "react";

class Employment extends Component {
  state = {
    aquent: "International Recruiting Agency",
    belkin: "Belkin",
    instructor: "Cerritos College / P.V. Art Center",
    medianews: "LA.com",
    dispDetails: 1,
    jobs: [
      {
        date: "2018 - Present",
        company: "California Parking Authority",
        link: "capa.html",
        title: "Head of Online Operations",
        duties: "Architect, build, manage and maintain the company’s administration portal (both front-end and back-end) used by all clients, field inspectors and partners. Oversee all web development, 3rp party integrations, and deployments. Manage offsite developers, data entry, and technical support team, and conduct online and onsite portal training sessions and demos. Support account executive team and provide reports to finance department.",
        achievements: [
          {
            title: "Build IT Department",
            descrip: "Learned everything there was about the parking enforcement industry and setup the company's first IT department.",
            tech: "JIRA, WorkBench"
          },
          {
            title: "Architect & Build Admin Portal",
            descrip: "Worked with the design and development team to automate 80% of the company's business processes.",
            tech: "Laravel, MySQL, jQuery, HTML, CSS, Invision"
          },
          {
            title: "Maintain & Enhance Admin Portal Features",
            descrip: "Maintained administration portal and conducted all training sessions.",
            tech: "PowerPoint, Ring Central"
          }
        ]
      },
      {
        date: "2017 - 2018",
        company: "K-Swiss Global Brands",
        link: "ksgb.html",
        title: "Web Development Manager",
        duties: "Development manager, frontend web development (HTML, CSS, jQuery, JavaScript), enterprise content management system, documentation, technical spec writing, vendor vetting, training, task management, process automation",
        achievements: [
          {
            title: "Manage Development Team",
            descrip: "Provided guidance and direction to a 4-person India development team in the completion of high priority backend development tasks to meet business needs.",
            tech: "JIRA, SKype"
          },
          {
            title: "Marketing Support",
            descrip: "Worked with online marketing managers to outline strategies and identify technical requirements. Then implemented marketing initiatives by building landing pages, responsive emails, collection databases, etc.",
            tech: "Laravel, jQuery, HTML, CSS"
          },
          {
            title: "Maitenance of e-Commerce Properties",
            descrip: "Maintained 4 brand websites (<em>Kswiss.com, Suprafootwear.com, Palladiumboots.com, OTZshoes.com</em>) accross the U.S., Europe and Australia. Sites were in 5 different languages and 3 different currencies.",
            tech: "Magento, Shopify"
          }
        ]
      },
      {
        date: "2013 - 2013",
        company: "Teleflora",
        link: "teleflora.html",
        title: "Web Producer",
        duties: "frontend web development (HTML, CSS, jQuery, JavaScript), enterprise content management system, documentation, technical spec writing, vendor vetting, training, task management, process automation",
        achievements: [
          {
            title: "Automated Email Generation",
            descrip: "Created an semi-automated process to generate responsive HTML emails cutting email coding from an average of 60 minutes to 5 minutes.",
            tech: "XML, XSL, Windows Shell"
          },
          {
            title: "Platform Lead",
            descrip: "As the lead of user documentation for the custom implementation of a new e-commerce platform, I led all major content updates and custom frontend features and functions.",
            tech: "ATG, Endeca, JavaScript, jQuery, HTML, CSS"
          },
          {
            title: "Vendor Expenditure Reduction",
            descrip: "Identified existing requirements and after a comprehensive vetting process replaced a legacy vendor with new one, a $90,0000 yearly savings.",
            tech: "Scene7, Cloudinary"
          }
        ],
        recommendations: [
          {
            name: "Michelle Farrell",
            thumb: "michellefarrell.jpg",
            link: "https://www.linkedin.com/in/michellefarrellseo/",
            date: "January 11, 2017",
            company: "SEO Consultant & Digital Virtual Assistant",
            quote: (
              <React.Fragment>
                I was always impressed by Jerry's leadership and project management skills when I worked with him at Teleflora. He was autonomous in establishing and streamlining policies, processes, and performance in support of continuous improvement across the website and in the delivery of assets from other departments. He was truly an evangelist who established standards in the company.
                <br />
                <br />
                He was my go-to person for any SEO technical improvements I want implemented. He was also very proactive and always consulted me when another department wanted to make website changes to make sure that the changes didn't negatively impact my SEO tactics. His solid work ethic and attention to detail in documenting processes made him a great asset not only to me as the SEO Manager but to the entire company. I highly recommend Jerry.
              </React.Fragment>
            )
          },
          {
            name: "Bryan Pan",
            thumb: "bryanpan.jpg",
            link: "https://www.linkedin.com/in/bryan-pan-140a0757/",
            date: "January 6, 2017",
            title: "Product Manager",
            company: "Bloomingdale's",
            website: "http://www.bloomingdales.com",
            quote: (
              <React.Fragment>
                Jerry is a unicorn. He has all the best traits you'd want in a developer:
                <br />
                <br />- Strong technical foundation, frontend and backend.
                <br />- Open to learning and embracing new technologies.
                <br />- Adaptable and able to find a concise path to the right solution.
                <br />- Not afraid to take initiative and willing to help others.
                <br />- Proactively finds problems and solutions before they become an issue.
                <br />
                <br />
                Best of all, he's easy to talk to and bounce ideas off of. He helped drive so many projects at Teleflora and was a pleasure to work with.
              </React.Fragment>
            )
          }
        ]
      },
      {
        date: "2008 - 2013",
        company: "GrowMyBizNet",
        title: "Web Developer",
        duties: (
          <React.Fragment>
            Fullstack web development. Clients/Projects included:
            <a href="http://verve8media.com" target="_blank">
              Verve 8 Media
            </a>
            ,
            <a href="http://www.cr8rec.com/" target="_blank">
              Creative Recreation
            </a>
            ,
            <a href="http://olympusamerica.com" target="_blank">
              Olympus Cameras
            </a>
            ,
            <a href="http://www.gsn.com/" target="_blank">
              Game Show Network
            </a>
            , Olympus Cameras, Kyocera,
            <a href="https://www.tsovet.com/" target="_blank">
              TSOVET
            </a>
            ,
            <a href="http://nartools.com" target="_blank">
              National Association of Realtors
            </a>
            , Drum-Squad,
            <a href="http://www.opengatecapital.com/" target="_blank">
              Open Gate Capital
            </a>
            ,
            <a href="http://www.omgglobal.com/" target="_blank">
              OMG Global
            </a>
            , NOSOTROS,
            <a href="http://pvartcenter.org/" target="_blank">
              P.V. Art Center
            </a>
            ,
            <a href="http://drmarion.com/" target="_blank">
              Dr. Marion
            </a>
            , Insomnia Media,
            <a href="http://www.thompsonchemists.com/" target="_blank">
              Thompson Chemists
            </a>
            ,
            <a href="http://www.help4srs.org/" target="_blank">
              H.E.L.P.
            </a>
            ,
            <a href="http://www.sigulerguff.com/" target="_blank">
              Siguler Guff
            </a>
          </React.Fragment>
        )
      },
      {
        date: "2011",
        company: "Belkin",
        title: "UX Prototyper",
        link: "http://belkin.com",
        duties: "Protoyping a web-based dashboard for pre-launch IoT electrical meter. (Technologies: JavaScript, jQuery)"
      },
      {
        date: "2009",
        company: "Cerritos College/P.V. Art Center",
        title: "Instructor",
        duties: 'Taught "Beginning Web Development" and "Introduction to Social Media".'
      },
      {
        date: "2007",
        company: "LA.com",
        title: "Project Manager",
        link: "http://la.com",
        duties: "Consolidating the entertainment section of 11 local newspapers into LA.com (Technologies: Velocity, cmPublish)",
        recommendations: [
          {
            name: "Tom Zevallos",
            thumb: "tomzevallos.jpg",
            link: "https://www.linkedin.com/in/tom-z-7931625/",
            date: "January 21, 2010",
            title: "Business Development Manager",
            company: "Conversant, Inc.",
            website: "http://www.conversantmedia.com",
            quote: (
              <React.Fragment>
                I've worked with Jerry at both the Torrance Daily Breeze and LA.com. Jerry is a well rounded developer with great communication skills. He has both the ability to effectively manage a technical team and bring understanding and support for a executive team.
                <br />
                <br />
                On several projects we worked on, he was able to build sites and businesses from the ground up through his unique problem solving and dedication. Jerry is the type of individual who can help clear out any fog from the technical side, and can deliver answers and results by presenting the options.
              </React.Fragment>
            )
          }
        ]
      }
    ]
  };

  /*************************
   * RENDER
   *************************/
  render() {
    return (
      <React.Fragment>
        <a href="#collapseEmployment" data-toggle="collapse" aria-controls="collapseEmployment" className="cv-header">
          <h2 className="title">
            <span>Employment</span>
          </h2>
          <span className="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>
          <span className="glyphicon glyphicon-triangle-bottom" style={{ display: "none" }} aria-hidden="true"></span>
        </a>
        <div className="white-space-10"></div>

        <ul id="collapseEmployment" className="resume-list collapse">
          {this.renderJobs()}
        </ul>
      </React.Fragment>
    );
  }

  /*************************
   * FUNCTIONS
   *************************/

  // FUNCTION renderJob //
  renderJobs() {
    return this.state.jobs.map((job, i) => (
      <li key={i}>
        <h4>
          <i className="fa fa-calendar ic-re"></i> {job.date}
        </h4>
        <i>{job.title}</i>
        <h3>
          <i className="fa fa-building-o ic-re"></i>
          {this.renderLink(job.link, job.company)}
        </h3>
        <p>
          <strong>Responsibilities included:</strong> {job.duties}
        </p>
        {job.achievements ? (
          <div className="achievements">
            <strong>Achievements</strong>
            <ul>
              {job.achievements.map((deed, c) => (
                <li key={c}>
                  <u>{deed.title}</u>
                  <br />
                  {deed.descrip}
                  <br />(<em>Technologies: {deed.tech}</em>)
                </li>
              ))}
            </ul>
          </div>
        ) : null}
        {job.recommendations ? (
          <React.Fragment>
            <a href={"#collapseRecomend" + i} data-toggle="collapse" aria-controls="collapseTeleflora" className="cv-testimonials collapsed" aria-expanded="false">
              <h4 className="title">
                <span>Recommendation</span>
              </h4>
              <span className="glyphicon glyphicon-triangle-right" aria-hidden="true" style={{ display: "inline-block" }}></span>
              <span className="glyphicon glyphicon-triangle-bottom" style={{ display: "none" }} aria-hidden="true"></span> <i>({job.recommendations.length})</i>
            </a>
            <ul id={"collapseRecomend" + i} className="list-unstyled list-testimonial clearfix collapse" aria-expanded="false" style={{ height: 0 }}>
              {this.renderRecommend(i)}
            </ul>
          </React.Fragment>
        ) : null}
      </li>
    ));
  }

  // FUNCTION renderLink //
  renderLink(pg, name) {
    const lnkIcon = <i className="fa fa-link"></i>;
    let link;
    if (pg == null) {
      link = <a className="nolink">{name}</a>;
    } else if (pg.search("http") >= 0) {
      link = (
        <a href={pg} target="_blank">
          {name} {lnkIcon}
        </a>
      );
    } else {
      link = (
        <a href={"/works/" + pg} className="link-work">
          {name} {lnkIcon}
        </a>
      );
    }

    return link;
  }

  // FUNCTION renderRecommend //
  renderRecommend(j) {
    return this.state.jobs[j].recommendations.map((recommend, i) => (
      <li key={i}>
        <img src={"/assets/theme/img/people/" + recommend.thumb} alt={recommend.name} className="img-thumbnail img-circle" />
        <div className="text-testimonial">
          <h3>
            <a href={recommend.link} target="_blank">
              <span className="fa fa-linkedin-square"></span> {recommend.name}
            </a>
            <br />
            <small>
              <i>Worked with Jerry - {recommend.date}</i>
            </small>
          </h3>
          <p>
            <sup>
              <span className="fa fa-quote-left"></span>
            </sup>
            &nbsp; {recommend.quote} &nbsp;
            <sup>
              <span className="fa fa-quote-right"></span>
            </sup>
          </p>
          <h4>
            {recommend.company}-
            <small>
              {recommend.title}
              <a href={recommend.website} target="_blank">
                <i className="fa fa-globe"></i> {recommend.website}
              </a>
            </small>
          </h4>
        </div>
      </li>
    ));
  }
}

export default Employment;
