import React, { Component } from "react";
import NavMain from "./navMain_";
import Menu from "./menu_";
import Hero from "./hero_";

class Header extends Component {
  render() {
    return (
      <header className="main-header">
        <NavMain />
        <Menu />
        <Hero />
      </header>
    );
  }
}

export default Header;
