import React, { Component } from "react";
import Entrepreneur from "./entrepreneur_";
import Employment from "./employment_";
import Education from "./education_";
import IBeMe from "./iBeMe_";
import Skills from "./skills_";

class Resume extends Component {
  render() {
    return (
      <section id="resume">
        <div className="section-padd">
          <div className="container-body clearfix">
            <br />
            <Employment />
            <Entrepreneur />
            <Education />
            <IBeMe />
            <Skills />
          </div>
        </div>
      </section>
    );
  }
}

export default Resume;
