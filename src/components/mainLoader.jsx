import React, { Component } from "react";

const MainLoader = () => {
  return (
    <div className="page-loader">
      <div className="v-align-center">
        <div className="middle-content">
          <div className="img-p-area">
            <img src="/assets/theme/img/profile-thumb.jpg" alt="" className="img-thumbnail no-radius" />
          </div>
          <span className="itsme">I be...</span>
          <h4>J.A.C.</h4>
          <p>please wait</p>
          <div className="anim-pg">
            <span></span>
          </div>
          <div className="force-pg">
            <button type="button" id="force-close-pg" className="btn">
              Skip This &rarr;
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MainLoader;
