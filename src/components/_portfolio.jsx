import React, { Component } from "react";
import PortfolioFilters from "./portfolioFilters_";
import PortfolioGrid from "./portfolioGrid_";
import Logos from "./logos_";

class Portfolio extends Component {
  render() {
    return (
      <section id="portfolio">
        <PortfolioFilters />
        <PortfolioGrid />
        <Logos />
      </section>
    );
  }
}

export default Portfolio;
