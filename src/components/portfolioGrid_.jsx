import React, { Component } from "react";

class PortfolioGrid extends Component {
  state = {
    projInfo: [
      {
        name: "Blowfish",
        tools: "WordPress",
        clss: "cms",
        pg: "https://blowfishshoes.com",
        interval: null,
        slider: [
          {
            img: "blowfish1.jpg",
            alt: "Blowfishshoes.com"
          }
        ]
      },
      {
        name: "CAPA",
        tools: "Management, Frontend, Backend, Other",
        clss: "fed bed other",
        pg: "capa.html",
        interval: 6050,
        slider: [
          {
            img: "capa1.jpg",
            alt: "Caliparking.com"
          },
          {
            img: "capa2.jpg",
            alt: "Violations"
          },
          {
            img: "capa3.jpg",
            alt: "Admin Portal"
          },
          {
            img: "capa4.jpg",
            alt: "Money Vault"
          }
        ]
      },
      {
        name: "4 KSGB Brands",
        tools: "Management, Frontend, Magento",
        clss: "fed cms other",
        pg: "ksgb.html",
        interval: null,
        slider: [
          {
            img: "ksgb1.jpg",
            alt: "Kswiss.com"
          },
          {
            img: "ksgb2.jpg",
            alt: "Suprafootwear.com"
          },
          {
            img: "ksgb3.jpg",
            alt: "Palladiumboots.com"
          },
          {
            img: "ksgb4.jpg",
            alt: "OTZshoes.com"
          }
        ]
      },
      {
        name: "Teleflora",
        tools: "Frontend, ATG",
        clss: "fed cms",
        pg: "teleflora.html",
        interval: 3010,
        slider: [
          {
            img: "teleflora1.jpg",
            alt: "Teleflora - Home Page"
          },
          {
            img: "teleflora2.jpg",
            alt: "Teleflora - Collection"
          }
        ]
      },
      {
        name: "Sample Spec Doc",
        tools: "Other",
        pg: "spec.html",
        clss: "other",
        interval: null,
        slider: [
          {
            img: "spec.png",
            alt: "Sample Spec Doc"
          }
        ]
      },
      {
        name: "Email Generator",
        tools: "Frontend, Backend, Other",
        clss: "fed other",
        pg: "email.html",
        interval: 4010,
        slider: [
          {
            img: "email1.jpg",
            alt: "Responsive Emails"
          },
          {
            img: "email2.jpg",
            alt: "Design View"
          },
          {
            img: "email3.jpg",
            alt: "Code View"
          }
        ]
      },
      {
        name: "LA.com",
        tools: "Frontend, cmPublish",
        clss: "fed cms",
        pg: null,
        interval: null,
        slider: [
          {
            img: "lacom.jpg",
            alt: "LA.com"
          }
        ]
      },
      {
        name: "Creative Recreation",
        tools: "Frontend, Backend, Magento",
        clss: "cms fed bed",
        pg: null,
        interval: null,
        slider: [
          {
            img: "cre8rec.jpg",
            alt: "Creative Recreation"
          }
        ]
      },
      {
        name: "OMG Global",
        tools: "Frontend, Drupal",
        clss: "fed cms bed",
        pg: null,
        interval: null,
        slider: [
          {
            img: "omgglobal.jpg",
            alt: "OMG Global"
          }
        ]
      },
      {
        name: "TSOVET",
        tools: "3D Cart",
        clss: "cms",
        pg: "https://tsovet.com",
        interval: null,
        slider: [
          {
            img: "tsovet.jpg",
            alt: "TSOVET"
          }
        ]
      },
      {
        name: "National Association of Realtors",
        tools: "Backend",
        clss: "bed",
        pg: null,
        interval: null,
        slider: [
          {
            img: "nar.jpg",
            alt: "NARTools"
          }
        ]
      },
      {
        name: "Palos Verdes Art Center",
        tools: "Business Catalyst",
        clss: "cms",
        pg: "https://pvartcenter.org",
        interval: null,
        slider: [
          {
            img: "pvac.jpg",
            alt: "P.V. Art Center"
          }
        ]
      },
      {
        name: "NightLifeConnect",
        tools: "Frontend, Backend, PHPfox",
        clss: "fed bed cms",
        pg: null,
        interval: null,
        slider: [
          {
            img: "nlc.jpg",
            alt: "NightLifeConnect"
          }
        ]
      },
      {
        name: "Thompson Chemists",
        tools: "Magento",
        clss: "cms",
        pg: null,
        interval: null,
        slider: [
          {
            img: "thompson.jpg",
            alt: "Thompson Chemists"
          }
        ]
      },
      {
        name: "Siguler Guff",
        tools: "Backend",
        clss: "bed",
        pg: "https://www.sigulerguff.com",
        interval: null,
        slider: [
          {
            img: "sigulerguff.jpg",
            alt: "Siguler Guff"
          }
        ]
      },
      {
        name: "Dr. Marion",
        tools: "Drupal",
        clss: "cms",
        pg: "http://drmarion.com",
        interval: null,
        slider: [
          {
            img: "drmarion.jpg",
            alt: "Dr. Marion"
          }
        ]
      }
    ]
  };

  render() {
    return <div className="galery-box clearfix bg2">{this.renderGrid()}</div>;
  }

  renderGrid() {
    const clss = "col-sm-3 col-xs-6 item-box";
    return this.state.projInfo.map((proj, i) => (
      <div className={proj.pg ? clss + " " + proj.clss : clss} key={i}>
        <div className="hover-area">
          <div className={proj.pg ? "text-vcenter-area hilite" : "text-vcenter-area"}>
            <div className="text-vcenter">
              <h3>{this.renderLink(proj.pg, proj.name)}</h3>
              <span>{proj.tools}</span>
            </div>
          </div>
        </div>
        <div id="bs-gslider1" className="carousel slide carousel-fade" data-ride="carousel" data-interval={proj.interval}>
          <div className="carousel-inner" role="listbox">
            {proj.slider.map((slide, s) => (
              <div className={s ? "item" : "item active"} key={s}>
                <img src={"/assets/theme/img/thumb/" + slide.img} alt={slide.alt} />
              </div>
            ))}
          </div>
        </div>
      </div>
    ));
  }

  renderLink(pg, name) {
    const lnkIcon = <i className="fa fa-link"></i>;
    let link;
    if (pg == null) {
      link = <a className="nolink">{name}</a>;
    } else if (pg.search("http") >= 0) {
      link = (
        <a href={pg} target="_blank">
          {name} {lnkIcon}
        </a>
      );
    } else {
      link = (
        <a href={"/works/" + pg} className="link-work">
          {name} {lnkIcon}
        </a>
      );
    }

    return link;
  }
}

export default PortfolioGrid;
