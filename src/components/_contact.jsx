import React, { Component } from "react";
import ContactForm from "./contactForm_";

class Contact extends Component {
  render() {
    return (
      <section id="contact">
        <div className="map-area  br-t br-b">
          <div className="map" id="map" data-lat="34.10" data-lng="-118.13"></div>
          <div className="map-scroll-space"></div>
          <div className="map-wait-msg">Please Wait...</div>
          <div className="map-detail-location">
            <h4>Location:</h4>
            <p>
              Alhambra, CA
              <br />
              U.S.A.
            </p>
          </div>
        </div>

        <ContactForm />
      </section>
    );
  }
}

export default Contact;
