import React, { Component } from "react";

class Logos extends Component {
  state = {
    clients: [
      {
        link: "http://www.kswiss.com",
        logo: "k-swiss.png",
        alt: "K-swiss"
      },
      {
        link: "http://www.teleflora.com",
        logo: "teleflora.png",
        alt: "Teleflora"
      },
      {
        link: "http://www.belkin.com",
        logo: "belkin.png",
        alt: "Belkin"
      },
      {
        link: "http://www.neutrogena.com",
        logo: "neutrogena.png",
        alt: "Neutrogena"
      },
      {
        link: "http://www.samsung.com",
        logo: "samsung.png",
        alt: "Samsung"
      },
      {
        link: "http://www.mazdausa.com",
        logo: "mazda.png",
        alt: "Mazda"
      },
      {
        logo: "isuzu.png",
        alt: "Isuzu"
      },
      {
        link: "http://www.olympusamerica.com",
        logo: "olympus.png",
        alt: "Olympus"
      }
    ]
  };

  render() {
    return (
      <div className="section-padd">
        <div className="container-body clearfix">
          <h2 className="title">
            <span>Past Projects/Clients</span>
          </h2>
          <div className="white-space-10"></div>
          <ul className="list-inline text-center list-force-img clearfix">{this.renderLogos()}</ul>
        </div>
      </div>
    );
  }

  renderLogos() {
    const path = "/assets/theme/img/patner/";
    return this.state.clients.map((client, i) => (
      <li key={i}>
        {client.link ? (
          <a href={client.link} target="_blank">
            <img src={path + client.logo} title={client.alt} alt={client.alt} />
          </a>
        ) : (
          <img src={path + client.logo} title={client.alt} alt={client.alt} />
        )}
      </li>
    ));
  }
}

export default Logos;
