import React, { Component } from "react";

class Hero extends Component {
  state = {
    dispHireBttns: 1
  };

  render() {
    return (
      <div className="hero-01">
        <div className="hero-border">
          <div className="top"></div>
          <div className="bottom"></div>
          <div className="left"></div>
          <div className="right">
            <div className="v-area">
              <div className="v-middle  show-span">
                <div className="p5" id="label-menu">
                  <span>A</span>
                  <span>B</span>
                  <span>O</span>
                  <span>U</span>
                  <span>T</span>
                  <span></span>
                  <span>M</span>
                  <span>E</span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="my-thumb">
          <div className="thumb-top">
            <img src="/assets/theme/img/profile-thumb.jpg" className="img-thumbnail no-radius" alt="J.A.C." />
          </div>
        </div>

        <div className="content-hero">
          <div className="v-content">
            <h4 className="font-normal">I be J.A.C.</h4>
            <h1 className="myname">Jerry A. Cornejo</h1>
            <h3 className="font-normal with-line">Senior Technologist & Entrepreneur</h3>
            {this.dispHireBttns()}
          </div>
        </div>
      </div>
    );
  }

  dispHireBttns() {
    const downlodBttn = (
      <p>
        <a href="#contact" className="btn btnc1">
          <span>Hire Me</span>
        </a>
        <a href="/assets/JCorResume.pdf" target="_blank" className="btn btnc1">
          <span>Download Resume</span>
        </a>
        <a href="#" target="_blank" className="bitbucket">
          <img src="/assets/theme/img/bitbucket.png" height="30" />
        </a>
      </p>
    );
    return this.state.dispHireBttns === 1 ? downlodBttn : "";
  }
}

export default Hero;
