import React, { Component } from "react";

const Menu = () => {
  return (
    <React.Fragment>
      <div className="over-fly-area" id="menu-ovefly">
        <div className="inner-overfly">
          <div className="middle-overfly">
            <h2 className="title-over">Menu</h2>
            <nav className="main-nav" id="menu">
              <ul className="nav">
                <li className="active">
                  <a href="#aboutme" className="inner-link" data-text="About Me">
                    <span>About Me</span>
                  </a>
                </li>
                <li>
                  <a href="#resume" className="inner-link" data-text="Curriculum vitae">
                    <span>Curriculum Vitae</span>
                  </a>
                </li>
                <li>
                  <a href="#portfolio" className="inner-link" data-text="My Portfolio">
                    <span>Portfolio</span>
                  </a>
                </li>
                <li>
                  <a href="#contact" className="inner-link" data-text="Contact Me">
                    <span>Contact</span>
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Menu;
