import React, { Component } from "react";

const NavMain = () => {
  return (
    <div className="nav-main">
      <span className="itsme">React</span>
      <a href="/?on=1" id="vStatic">
        <span className="itsme not-selected btnc1">
          <span>Static</span>
        </span>
      </a>
      <a href="/angular1/#/?on=1" id="vAngular">
        <span className="itsme not-selected btnc1">
          <span>Angular 1</span>
        </span>
      </a>
      <a id="vVue">
        <span className="itsme not-selected btnc1">
          <span>Vue</span>&nbsp;
          <span style={{ fontSize: 10 }}>(TBD)</span>
        </span>
      </a>
      <a href="#menu-ovefly" className="toogle-overfly">
        <i className="fa fa-bars open-t"></i> <i className="fa fa-times close-t"></i>
      </a>
    </div>
  );
};

export default NavMain;
