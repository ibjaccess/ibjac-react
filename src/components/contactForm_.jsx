import React, { Component } from "react";

class ContactForm extends Component {
  state = {};
  render() {
    return (
      <div className="section-padd">
        <div className="container-body clearfix">
          <h2 className="title dark">
            <span>
              Tell Me About: <small id="show-subject">...</small>{" "}
              <small>
                <a href="#" className="fire-toogle-subject mini-arrow">
                  <i className="fa fa-angle-down"></i>
                </a>
              </small>
            </span>
          </h2>
          <div className="dropdown top-min">
            <a href="#" data-toggle="dropdown" id="toogle-subject"></a>
            <ul className="dropdown-menu" id="option_subject">
              <li>
                <a href="#">Your Next Project</a>
              </li>
              <li>
                <a href="#">Your Upcoming Conference</a>
              </li>
              <li>
                <a href="#">Your Dev Training Program</a>
              </li>
              <li>
                <a href="#">How We Met</a>
              </li>
            </ul>
          </div>
          <div className="white-space-10"></div>
          <form method="post" id="ContactForm" name="contactForm" className="validate-form">
            <input type="hidden" name="subject" value="" id="subject_contact" />
            <input type="hidden" name="file" id="file-att" value="" />
            <div className="form-group">
              <label>Your Name (*)</label>
              <input type="text" className="form-control form-flat" name="fullname" required />
            </div>
            <div className="form-group">
              <label>Email (*)</label>
              <input type="email" className="form-control form-flat" name="email" required />
            </div>
            <div className="form-group">
              <label>Your Message (*)</label>
              <textarea className="form-control form-flat" name="message" rows="8" required></textarea>
            </div>

            <div className="hold-feature uploader-hold">
              <div className="form-group">
                <label>
                  Attach Your Document (Optional) <span className="display-block ">(only .pdf allowed , max size 2Mb)</span>
                </label>
                <div className="clearfix">
                  <input type="button" id="upload-btn" className="btn  btn-file btn-xs btn-default clearfix" value="Choose file" />
                  <div id="errormsg" className="clearfix error"></div>
                  <div id="pic-progress-wrap" className="progress-wrap"></div>
                  <div id="picbox" className="attbox "></div>
                </div>
              </div>
            </div>

            <div className="hold-feature captcha-hold">
              <div className="form-group">
                <div id="mycaptcha-wrap" className="mycaptcha1">
                  <div id="mycaptcha" className="mycaptcha1"></div>
                </div>
              </div>
            </div>

            <div className="form-group">
              <div className="preload-submit hidden">
                <hr /> <i className="fa fa-spinner fa-spin"></i> Please Wait ...
              </div>
              <div className="message-submit error hidden"></div>
            </div>

            <div className="form-group">
              <button type="submit" className="btn btnc2 with-br ">
                <span>Send This Message</span>
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default ContactForm;
