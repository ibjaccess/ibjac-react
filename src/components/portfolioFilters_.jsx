import React, { Component } from "react";

class PortfolioFilters extends Component {
  render() {
    return (
      <div className="section-padd br-t bg2">
        <div className="container-body clearfix">
          <h2 className="title dark">
            <span>Some Works</span>
          </h2>
          <ul className="list-inline list-filter-galery">
            <li className="active" data-filter="*">
              <a href="#">All</a>
            </li>
            <li data-filter=".fed">
              <a href="#">Frontend</a>
            </li>
            <li data-filter=".bed">
              <a href="#">Backend</a>
            </li>
            <li data-filter=".cms">
              <a href="#">CMS</a>
            </li>
            <li data-filter=".other">
              <a href="#">Other</a>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default PortfolioFilters;
