import React, { Component } from "react";

class Education extends Component {
  state = {
    instits: [
      {
        result: "Founder",
        name: "Founder Institute",
        descrip: "The Founder Institute is the world's premier startup boot camp for talented entrepreneurs. Through a structured, 4-month curriculum of weekly training courses and business-building assignments, graduates of the Founder Institute come out as Founders of enduring companies.",
        link: "http://fi.co"
      },
      {
        result: "Business Management Certificate",
        name: "California Institute of Business Management"
      },
      {
        result: "Computer Science Undergrad",
        name: "California State University Dominguez Hills"
      }
    ]
  };

  render() {
    return (
      <React.Fragment>
        <a href="#collapseEducation" data-toggle="collapse" aria-controls="collapseEducation" className="cv-header">
          <h2 className="title">
            <span>Education</span>
          </h2>
          <span className="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>
          <span className="glyphicon glyphicon-triangle-bottom" style={{ display: "none" }} aria-hidden="true"></span>
        </a>

        <div className="white-space-10"></div>
        <ul id="collapseEducation" className="resume-list collapse">
          {this.renderInstitutes()}
        </ul>
      </React.Fragment>
    );
  }

  renderInstitutes() {
    return this.state.instits.map((inst, i) => (
      <li key={i}>
        <h4>
          <i className="fa fa-calendar ic-re"></i>
        </h4>
        <i>{inst.result}</i>
        <h3>
          <i className="fa fa-building-o ic-re"></i>{" "}
          {inst.link ? (
            <a href={inst.link} target="_blank">
              {inst.name}
            </a>
          ) : (
            inst.name
          )}
        </h3>
        <p>{inst.descrip}</p>
      </li>
    ));
  }
}

export default Education;
