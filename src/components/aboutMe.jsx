import React, { Component } from "react";

class AboutMe extends Component {
  state = {
    titles: ["Senior Technologist", "Entrepreneur"],
    subtitles: [
      {
        name: "Web Engineer",
        descrip: "Architect and build web sites and applications."
      },
      {
        name: "Efficiency Analyst",
        descrip: "Analyze, identify, improve, and repeat."
      },
      {
        name: "Team Lead",
        descrip: "Apply individual capabilities to specific needs."
      },
      {
        name: "Site Optimization",
        descrip: "Load time, cross-browser, SEO, and more."
      },
      {
        name: "Technical Writer",
        descrip: "Gather requirements, be concise, provide use cases."
      },
      {
        name: "Vendor/Talent Vetting",
        descrip: "Know your audience, ask the right questions."
      }
    ]
  };

  render() {
    return (
      <section id="aboutme">
        <div className="section-padd bg2">
          <div className="container-body clearfix">
            <div className="big-qoute">
              <h3>
                <em>"Start where you are. Use what you have. Do what you can."</em>
                <br />
                <br />
                <small>&rarr; Arthur Ashe (by way of Theodore Roosevelt)</small>
              </h3>
            </div>
          </div>
        </div>
        <div className="section-padd top-bold-border">
          <div className="container-body clearfix">
            <div className="row">
              <div className="col-md-12">
                <h2 className="title">
                  <span>Who be J.A.C</span>
                </h2>
                <p>An accomplished web-business architect and strategist with close to 20 years of web development experience. A strong "hybrid" skill-set encompassing leadership, architecture and both frontend and backend development. Highly regarded by business partners and executive teams as a key advisor, creative problem solver, skilled leader, and astute process and business analyst.</p>
              </div>
            </div>
          </div>
        </div>
        <div className="clearfix br-t">{this.renderTitles()}</div>
        <div className="bg2 p30 br-b">
          <div className="container-body clearfix">
            <div className="clearfix br-t br-r ">{this.renderSubTitles()}</div>
          </div>
        </div>
      </section>
    );
  }

  renderTitles() {
    return this.state.titles.map((title, i) => (
      <div className="col-md-6 col-sm-6 no-padding" key={i}>
        <div className="desc-mini no-br-l">
          <div className="mid-desc-mini">
            <h3>{title}</h3>
          </div>
        </div>
      </div>
    ));
  }

  renderSubTitles() {
    return this.state.subtitles.map((subtit, i) => (
      <div className="col-lg-4 col-md-6 col-sm-6 no-padding" key={i}>
        <div className="desc-mini">
          <div className="mid-desc-mini">
            <h4>{subtit.name}</h4>
            <p>{subtit.descrip}</p>
          </div>
        </div>
      </div>
    ));
  }
}

export default AboutMe;
