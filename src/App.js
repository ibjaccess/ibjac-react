import React from "react";
import MainLoader from "./components/mainLoader";
import Header from "./components/_header";
import AboutMe from "./components/aboutMe";
import Resume from "./components/_resume";
import Portfolio from "./components/_portfolio";
import Contact from "./components/_contact";

function App() {
  const d = new Date();
  const yr = d.getFullYear();

  return (
    <React.Fragment>
      <MainLoader />

      <div className="body-area">
        <Header />

        <div className="content-body">
          <AboutMe />
          <Resume />
          <Portfolio />
          <Contact />
        </div>

        <footer className="main-footer">&copy; {yr} I be J.A.C.</footer>
      </div>
      <div className="over-fly-area" id="load-works">
        <div className="inner-overfly" id="work-wait-msg">
          <div className="middle-overfly">
            <h2 className="title-over">PLEASE WAIT...</h2>
          </div>
        </div>
        <div className="work-close">
          <a href="#" className="close-panel-work btn btn-xs btn-default">
            Close
          </a>
        </div>
        <div id="load-work-html"></div>
      </div>
    </React.Fragment>
  );
}

export default App;
